const http = require('http');

module.exports = function (app, startFn, closeFn) {
  require('./server/routes')(app);

  require('console-stamp')(console, 'HH:MM:ss.l');

  // error handling
  if (process.env.NODE_ENV === 'production') {
    // only use in development
    app.use(function (err, req, res, next) {
      if(!err) return next();
      console.error(err.stack);
      res.sendStatus(500);
    });
  }

  const server = http.createServer(app);

  server.listen(app.get('port'), function () {
    console.log(`Server is listening on port ${app.get('port')}. NODE_ENV=${process.env.NODE_ENV}`);
    if (startFn) startFn(server);
  });

  server.on('close', function () {
    console.log('closing server');
    if (closeFn) closeFn();
  });
};
