module.exports = {
  SHAPE: 'circle',
  COLOUR: '#555555',
  COLOUR_DARK: '#383838',
  LABEL_COLOUR: '#2A2A2A',
  TREE_TYPE: 'radial',
  NODE_SIZE: 10,
  LABEL_SIZE: 10,
  LAYOUT: {
    MINIMUM_CONTAINER_WIDTH: 150,
    MINIMUM_CONTAINER_HEIGHT: 50,
  },
  SELECTED_TREE_NODE_LABEL: 'id',
  MAP: {
    CENTER: {
      LATITUDE: 47.34452036,
      LONGITUDE: 5.85082183,
    },
    ZOOM: 4,
  },
  PANE_NAMES: {
    MAP: 'map',
    TREE: 'tree',
    DATA: 'data',
  },
  COMPONENT_NAMES: {
    ALL: 'All',
    MAP: 'Map',
    TREE: 'Tree',
    TIMELINE: 'Timeline',
    SEARCH: 'Search',
    LEGEND: 'Legend',
  },
  THEME: {
    COLOURS: {
      GREEN: '#3C7383',
    },
    ICONS: {
      MAP: 'language',
      TREE: 'nature',
      TIMELINE: 'access_time',
      DOWNLOAD: 'file_download',
      RESET: 'refresh',
      SHARE: 'share',
    },
  },
  LONG_PRESS_DURATION: 1000,
};
