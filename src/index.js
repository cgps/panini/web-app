import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';

import Application from './components/Application.react';

import configUtils from '^/utils/config';

module.exports = function (config = {}) {
  configUtils.set(config);
  ReactDOM.render((
      <Router history={browserHistory}>
        <Route path="*" component={Application} />
      </Router>
    ),
    document.getElementById('application-placeholder')
  );
};
