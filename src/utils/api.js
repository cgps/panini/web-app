const apiUrl = '/api/1.0';

const REQUEST_TYPES = {
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
};

function ajax(type, path, data) {
  return new Promise((resolve, reject) => {
    window.jQuery.ajax({
      type,
      data: JSON.stringify(data, null, 4),
      dataType: 'json',
      url: `${apiUrl}${path}`,
      contentType: 'application/json; charset=UTF-8',
    })
    .then(res => resolve(res))
    .catch(err => reject(err.responseJSON ? err.responseJSON.error : err.responseText));
  });
}

// function ajax(type, url, data) {
//   return new Promise((resolve, reject) => {
//     const xhr = new XMLHttpRequest();
//     xhr.open(type, url, true);
//
//     xhr.onload = function (res) {
//       resolve(res);
//     };
//
//     xhr.onerror = function (err) {
//       reject(err);
//     };
//
//     xhr.setRequestHeader('Content-Type', 'text/plain; charset=UTF-8');
//
//     console.log('data', data);
//
//     xhr.send(data);
//   });
// }

module.exports = function ({ data, perplexity, theta }) {
  // console.log('[api]', 'file', fileHandle);
  return ajax(
    REQUEST_TYPES.POST,
    `/rtab?p=${perplexity}&t=${theta}`,
    data
  );
};
