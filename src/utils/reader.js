/* global FileReaderSync */

const BYTES_PER_MB = 1024 * 1024;
const FILE_LIMIT = 512;

// function readFile(handle, callback) {
//   if (typeof FileReader === 'undefined') {
//     try {
//       const reader = new FileReaderSync();
//       callback(null, reader.readAsText(handle));
//     } catch (e) {
//       callback(`FileReaderSync failed to read file: ${e}`);
//     }
//   } else {
//     const fileReader = new FileReader();
//     fileReader.onload = function handleLoad(event) {
//       callback(null, event.target.result);
//     };
//     fileReader.onerror = function handleError(e) {
//       callback(`FileReader failed to read file: ${e}`);
//     };
//     fileReader.readAsText(handle);
//   }
// }

module.exports = function (handle) {
  return new Promise((resolve, reject) => {
    console.log('[file-reader]', handle);
    if (handle.size > FILE_LIMIT * BYTES_PER_MB) {
      reject(`Rtab file must be smaller than ${FILE_LIMIT}`);
      return;
    }
    // readFile(handle, (err, data) => {
    //   if (err) {
    //     reject(err);
    //   }
    //   console.log('[file-reader] onload', event);
    //   resolve(data);
    // });
    const fileReader = new FileReader();
    fileReader.onload = function handleLoad(event) {
      console.log('[file-reader] onload', event);
      resolve(event.target.result);
    };
    fileReader.onerror = function handleError(e) {
      console.error('[file-reader]', e);
      reject(`FileReader failed to read file: ${e}`);
    };
    fileReader.readAsText(handle);
  });
};
