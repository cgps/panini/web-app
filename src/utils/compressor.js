const JSZip = require('jszip');

module.exports = function (data) {
  const zip = new JSZip();
  zip.file('rtab.txt', data);
  return zip.generateAsync(
    { type: 'base64', compression: 'DEFLATE' }
  );
};
