import reader from './reader';
import compressor from './compressor';
import api from './api';

module.exports = function ({ fileHandle, perplexity, theta }, updateProgress) {
  console.log('[file-processor]', 'Reading file');

  // updateProgress('Uploading file to server...');
  // return api({ fileHandle, perplexity, theta });

  updateProgress('Reading Rtab file...');
  return (
    reader(fileHandle)
    .then(data => {
      console.log('[file-processor]', 'File read', 'File length', data.length);
      if (data.length === 0) {
        console.error('Empty file contents received.');
        throw new Error('Empty file contents received.');
      }
      console.log('[file-processor]', 'Compressing file');
      updateProgress('Compressing file');
      return compressor(data);
    })
    .then(zip => {
      console.log('[file-processor]', 'File compressed');
      console.log('[file-processor]', 'Uploading file');
      updateProgress('Processing file on server...');
      return (
        api({ data: { zip }, perplexity, theta })
          .then(results => ({ zip, results }))
      );
    })
  );
};
