import '^/css/application.css';

import React from 'react';

import Appbar from 'muicss/lib/react/appbar';
import Container from 'muicss/lib/react/container';
import Button from 'muicss/lib/react/button';

import Upload from './Upload.react';

export default React.createClass({

  displayName: 'Application',

  render() {
    return (
      <div className="application">
        <Appbar className="application-header">
          <div className="mui-container mui--appbar-height mui--appbar-line-height">
            <div className="application-title">
              PANINI
            </div>
            <div className="application-links">
              <a className="mui-btn" href="/">Home</a>
              <a className="mui-btn" href="/" target="_blank" rel="nopener">Tutorial</a>
              <a className="mui-btn" href="https://gitlab.com/cgps/panini" target="_blank" rel="nopener">Gitlab</a>
            </div>
          </div>
        </Appbar>
        <div className="application-container">
          <div className="mui--appbar-height"></div><br />
          <Container>
            <Upload />
          </Container>
        </div>
        <footer>
          <div className="mui-container mui--text-center">
            Developed by <a href="http://www.pathogensurveillance.net/" target="_blank" rel="nopener">The Centre for Genomic Pathogen Surveillance (CGPS)</a>
          </div>
        </footer>
      </div>
    );
  },

});
