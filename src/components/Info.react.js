import '^/css/info.css';

import React from 'react';

export default React.createClass({

  displayName: 'Info',

  onClick(event) {
    event.preventDefault();
    $('input[type="file"]').click();
  },

  render() {
    return (
      <div className="mui--text-center application-info">
        <div className="mui--text-display3">PANINI</div>
        <div className="mui--text-display2">
          Pangenome Neighbor Identification <br />
          for Bacterial Populations <br />
          using <a href="https://github.com/lvdmaaten/bhtsne" target="_blank">Barnes-Hut t-SNE</a>
        </div>
        <div className="mui--text-display1">
          Drag and drop a <b>.rtab</b> file
          or <a href="" onClick={this.onClick}> select file</a> to start
        </div>
      </div>
    );
  },

});
