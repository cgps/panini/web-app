import React from 'react';
import Panel from 'muicss/lib/react/panel';
import Button from 'muicss/lib/react/button';

export default function({ files, onUploadFiles }) {
  return (
    <div className="mp-info-list">
      <div className="mui--text-display1">
        Drag and drop more files or click upload
        <div className="mui--pull-right">
          <Button size="large" color="primary" variant="raised" onClick={onUploadFiles}>UPLOAD</Button>
        </div>
      </div>
      <Panel>
        <table className="mui-table mui-table--bordered">
          <thead>
            <tr>
              <td>File</td>
              <td>Status</td>
            </tr>
          </thead>
          <tbody>
            {
              files.map((file, index) => (
                <tr key={index}>
                  <td>{file.name}</td>
                  <td>{file.status}</td>
                </tr>
              ))
            }
          </tbody>
        </table>
      </Panel>
    </div>
  );
};
