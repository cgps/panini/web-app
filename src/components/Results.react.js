/* global vis */

import React from 'react';
import Panel from 'muicss/lib/react/panel';
import Button from 'muicss/lib/react/button';

import Network from './Network.react';

import configUtils from '^/utils/config';

function exportFile(data, fileType, filename) {
  const dataUrl = `data:${fileType};utf8,${encodeURIComponent(data)}`;
  const anchor = document.createElement('a');
  if (typeof anchor.download !== 'undefined') {
    anchor.download = `panini-${filename}`;
    anchor.href = dataUrl;
    anchor.dataset.downloadurl =
      [ fileType, anchor.download, anchor.href ].join(':');
    document.body.appendChild(anchor);
    anchor.click();
    document.body.removeChild(anchor);
  } else {
    document.location.href = dataUrl;
  }
}

function generateCsv(rows) {
  return [ 'index,id,x,y' ].concat(
    rows.map(({ id, x, y }, index) => [ index, id, x, y ].join(','))
  ).join('\n');
}

function downloadCsv(rows, filename) {
  exportFile(generateCsv(rows), 'text/csv', `${filename}.csv`);
}

function generateDot(rows) {
  const SCALE = 32;
  const lines = rows.map(({ id, x, y }) => `"${id}"[x=${parseFloat(x) * SCALE},y=${parseFloat(y) * SCALE}]`);
  return `graph G { ${lines.join('; ')} }`;
}

function downloadDot(rows, filename) {
  exportFile(generateDot(rows), 'text/dot', `${filename}.dot`);
}

function downloadJson(rows, filename) {
  exportFile(JSON.stringify(rows, null, '  '), 'application/json', `${filename}.json`);
}

export default React.createClass({

  displayName: 'Results',

  propTypes: {
    rows: React.PropTypes.array.isRequired,
    compressedRtab: React.PropTypes.string.isRequired,
    filename: React.PropTypes.string,
  },

  render() {
    const { rows, filename, compressedRtab } = this.props;
    const microreactUrl = configUtils.get('microreactUrl');
    return (
      <Panel className="app-results">
        <form className="" ref="form" action={`${microreactUrl}/upload`} method="post" target="_blank">
          <Button type="button" size="large" variant="raised" onClick={() => downloadCsv(rows, filename)}>
            <img src="/images/icon-csv.svg" /> Download CSV
          </Button>
          <Button type="button" size="large" variant="raised" onClick={() => downloadDot(rows, filename)}>
            <img src="/images/icon-dot.svg" /> Download DOT
          </Button>
          <Button type="button" size="large" variant="raised" onClick={() => downloadJson(rows, filename)}>
            <img src="/images/icon-json.svg" /> Download JSON
          </Button>
          <Button type="submit" size="large" variant="raised">
            <img src="https://microreact.org/images/favicon-32x32.png" /> Send to Microreact
          </Button>
          <input type="hidden" name="network" value={generateDot(rows)} />
          <input type="hidden" name="files" value={`genes:text/rtab;base64,${compressedRtab}`} />
        </form>
        <Network rows={rows} />
      </Panel>
    );
  },

});
