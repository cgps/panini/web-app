import '^/css/cgps-file-upload.css';
import '^/css/upload.css';

import 'jquery-draghover';

import React from 'react';
import Panel from 'muicss/lib/react/panel';
import Button from 'muicss/lib/react/button';
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';

import DragAndDrop from './DragAndDrop.react';
import Info from './Info.react';
import Results from './Results.react';

import fileProcessor from '../utils/file';

const supportedFileTypes = [ 'rtab' ];

function checkFileType(filename) {
  for (const filetype of supportedFileTypes) {
    if (filename.indexOf(filetype, filename.length - filetype.length) !== -1) {
      return true;
    }
  }
  return false;
}

export default React.createClass({

  displayName: 'Upload',

  propTypes: {
    className: React.PropTypes.string,
  },

  getInitialState() {
    return {
      error: null,
      isProcessing: false,
      message: null,
      results: null,
      file: null,
      paramsP: 0,
      paramsMaxP: 100,
      paramsTheta: 0.5,
    };
  },

  componentDidMount() {
  },

  onFileInputChange(event) {
    event.preventDefault();
    if (event && event.target && event.target.files) {
      this.queueFiles(Array.from(event.target.files));
    }
  },

  resetUpload() {
    this.setState(this.getInitialState());
  },

  queueFiles(fileHandlers) {
    if (fileHandlers.length !== 1) {
      this.setState({ error: 'One .rtab file is required' });
      return;
    }

    const handler = fileHandlers[0];
    const name = handler.name;
    if (checkFileType(name.toLowerCase())) {
      this.setState({ file: { name, handler } });
    } else {
      this.setState({ error: 'File type is not supported' });
    }
  },

  processFile() {
    this.setState({
      isProcessing: true,
      results: [],
    });
    const { file, paramsP, paramsTheta } = this.state;
    const { handler } = file;
    fileProcessor(
      {
        fileHandle: handler,
        perplexity: paramsP === 0 ? '' : paramsP,
        theta: paramsTheta,
      },
      message => this.setState({ message })
    )
    .then(({ results, zip }) => {
      this.setState({ isProcessing: false, results, zip });
    })
    .catch(err => {
      this.setState({
        isProcessing: false,
        error: err.message || err,
      });
    });
  },

  renderContent() {
    const { error, isProcessing, message, file, results, zip,
            paramsP, paramsMaxP, paramsTheta } = this.state;

    if (error) {
      return (
        <Panel className="error-panel">
          <h2>Please fix the following errors and try again</h2>
          <p className="mui--text-center">
            { error }
          </p>
          <div className="mui--text-right">
            <Button color="primary" variant="raised" onClick={this.resetUpload}>Try again</Button>
          </div>
        </Panel>
      );
    }

    if (isProcessing) {
      return (
        <div className="loading">
        <h1>{ message }...</h1>
          <img src={`/images/loading-${(Math.random() * 16).toFixed()}.svg`} />
        </div>
      );
    }

    if (results) {
      return (
        <Results rows={results} compressedRtab={zip} filename={file.name || 'rtab'} />
      );
    }

    if (file !== null) {
      return (
        <Panel className="parameters-panel">
          <Form>
            <legend>Rtab file: { file.name }</legend>
            <Input label={`Perplexity (p) = ${paramsP === 0 ? 'auto' : paramsP}`} required
              type="range" min="0" max={paramsMaxP} step="1" value={paramsP}
              onChange={(e) => this.setState({ paramsP: parseInt(e.target.value, 10) })}
            />
            <Input label={`Gradient accuracy (theta) = ${paramsTheta}`} required
              type="range" min="0" max="1" step="0.1" value={paramsTheta}
              onChange={(e) => this.setState({ paramsTheta: e.target.value })}
            />
            <Button type="button" size="large" variant="raised" onClick={this.processFile}>
              <img src="/images/icon-upload.svg" /> Upload
            </Button>
          </Form>
        </Panel>
      );
    }

    return (<Info />);
  },

  render() {
    return (
      <DragAndDrop onDrop={files => this.queueFiles(files)}>
        { this.renderContent() }
        <input
          ref="fileInput"
          type="file"
          accept={supportedFileTypes.join(',')}
          className="cgps-file-upload"
          onChange={this.onFileInputChange}
        />
      </DragAndDrop>
    );
  },

});
