/* global vis */

import React from 'react';

const { Network, DataSet } = vis;

const visOptions = {
  configure: {
    enabled: false,
  },
  layout: {
    randomSeed: 0,
  },
  interaction: {
    dragNodes: false,
    dragView: true,
    hover: true,
    hoverConnectedEdges: true,
    multiselect: true,
    selectable: true,
    selectConnectedEdges: true,
    zoomView: true,
    navigationButtons: true,
  },
};
function getMouseWheelDelta(event) {
  // retrieve delta
  let delta = 0;
  if (event.wheelDelta) { /* IE/Opera. */
    delta = event.wheelDelta / 120;
  } else if (event.detail) { /* Mozilla case. */
    // In Mozilla, sign of delta is different than in IE.
    // Also, delta is multiple of 3.
    delta = -event.detail / 3;
  }
  // If delta is nonzero, handle it.
  // Basically, delta is now positive if wheel was scrolled up,
  // and negative, if wheel was scrolled down.
  return delta;
}

export default React.createClass({

  displayName: 'Network',

  propTypes: {
    rows: React.PropTypes.array.isRequired,
  },

  componentDidMount() {
    // create vis datasets for nodes and edges and save its reference
    const nodes = new DataSet(
      this.props.rows.map(({ id, x, y }) => ({
        title: id,
        size: 8,
        fixed: true,
        x,
        y,
      }))
    );
    const edges = new DataSet([]);
    this.dataSets = { nodes, edges };

    // initialise vis network and save its reference
    this.network = new Network(this.refs.network, this.dataSets, visOptions);

    // this.network.on('doubleClick', event => {
    //   const isMulti = event.event.pointers[0].metaKey;
    //   const clickedNodes = this.findNodesAtPointer(event.pointer);
    //   if (clickedNodes.length > 0) {
    //     const ids = [];
    //     for (const node of clickedNodes) {
    //       ids.push(node.id);
    //       this.dataSets.edges.forEach(edge => {
    //         if (edge.from === node.id) ids.push(edge.to);
    //         if (edge.to === node.id) ids.push(edge.from);
    //       });
    //     }
    //     if (isMulti) {
    //       for (const id of this.props.highlightedIds) {
    //         if (!ids.includes(id)) {
    //           ids.push(id);
    //         }
    //       }
    //     }
    //     this.props.setHighlightedIds(ids);
    //   }
    // });

    // this.network.on('click', event => {
    //   const isMulti = event.event.pointers[0].metaKey || event.event.pointers[0].ctrlKey;
    //   const clickedNodes = this.findNodesAtPointer(event.pointer);
    //   if (!isMulti || clickedNodes.length > 0) {
    //     this.props.setHighlightedIds(clickedNodes.map(n => n.id), isMulti);
    //   }
    // });

    const onMouseWheel = this.network.body.eventListeners.onMouseWheel;
    this.network.body.eventListeners.onMouseWheel = (event) => {
      if (event.metaKey || event.ctrlKey) {
        const delta = getMouseWheelDelta(event);
        if (delta !== 0) {
          this.expandNodes(getMouseWheelDelta(event) > 0 ? +1 : -1);
        }
      } else {
        onMouseWheel.call(this.network.interactionHandler, event);
      }
      event.preventDefault();
    };

    this.expandNodes(5);
  },

  componentWillUnmount() {
    this.network.destroy();
  },

  findNodesAtPointer(pointer) {
    const { x, y } = pointer.canvas;
    const nodes = [];
    for (const nodeId of this.network.body.nodeIndices) {
      const node = this.network.body.nodes[nodeId];
      if (x >= (node.x - node.baseSize) && x <= (node.x + node.baseSize) &&
          y >= (node.y - node.baseSize) && y <= (node.y + node.baseSize)) {
        nodes.push(node);
      }
    }
    return nodes;
  },

  expandNodes(sign) {
    const factor = Math.pow(2, sign);
    for (const nodeId of this.network.body.nodeIndices) {
      const node = this.dataSets.nodes._data[nodeId];
      node.x = node.x * factor;
      node.y = node.y * factor;
      this.dataSets.nodes.update(node);
    }
    this.network.fit();
  },

  render() {
    return (
      <div className="network-vis" ref="network"></div>
    );
  },

});
