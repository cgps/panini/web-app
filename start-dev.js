const webpack = require('webpack');
const webpackConfig = require('./webpack.config');
const webpackCompiler = webpack(webpackConfig);
const webpackDevMiddleware = require('webpack-dev-middleware');

require('./server')(function (error, app) {
  if (error) {
    console.log('Application not started:');
    throw error;
  }

  app.use(
    webpackDevMiddleware(webpackCompiler, {
      contentBase: '/public',
      publicPath: webpackConfig.output.publicPath,
      stats: {
        colors: true,
        cached: false,
      },
    })
  );

  app.use(require('webpack-hot-middleware')(webpackCompiler));

  app.use('/styles.css', function (req, res) {
    res.send('');
  });

  require('./start')(app);
});
