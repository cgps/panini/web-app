Rtab to JSON:

  curl -H "Content-type: text/plain; charset=UTF-8" -X POST --data-binary @gene_presence_absence.Rtab https://panini.wgsa.net/api/1.0/rtab

Rtab to DOT:

    curl -H "Content-type: text/plain; charset=UTF-8" -X POST --data-binary @gene_presence_absence.Rtab https://panini.wgsa.net/api/1.0/rtab\?o\=dot
