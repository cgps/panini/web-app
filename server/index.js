const express = require('express');
const bodyParser = require('body-parser');

const hostname = process.env.HOSTNAME || 'localhost';
const port = process.env.PORT || '8080';
const app = express();

app.config = {};

app.set('hostname', hostname);
app.set('port', port);
// http://stackoverflow.com/a/19965089
app.use(
  bodyParser.text({ limit: '512mb' })
);
app.use(
  bodyParser.urlencoded({ extended: true, limit: '512mb' })
);
app.use(
  bodyParser.json({ limit: '512mb' })
);

// set the view engine to ejs
app.set('view engine', 'ejs');

module.exports = function (callback) {
  callback(null, app);
};

