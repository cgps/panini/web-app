const jszip = require('jszip');
const express = require('express');

const bhtsne = require('../utils/bhtsne');
const createGraph = require('../utils/generate-graph');

function getRequestParams(req) {
  if (!req.body) throw new Error('Request body is empty.');

  const contentType = (req.headers['content-type'] || 'text/plain').toLowerCase();
  const perplexity = req.query.perplexity || req.query.p;
  const theta = req.query.theta || req.query.t;
  const outputFormat = req.query.output || req.query.o;

  return new Promise(resolve => {
    if (contentType.includes('application/json')) {
      if (req.body.file) {
        return resolve(req.body.file);
      }

      if (req.body.zip) {
        return (
          jszip.loadAsync(req.body.zip, { base64: true, compression: 'DEFLATE' })
            .then(zip => zip.file('rtab.txt').async('string'))
            .then(unzipped => resolve(unzipped))
        );
      }

      throw new Error('Request body does not include a `file` or `zip`.');
    }

    if (contentType.includes('text/plain')) {
      return resolve(req.body);
    }

    throw new Error('Invalid request content type.');
  }).then(rtabMatrix => ({
    rtabMatrix,
    perplexity: perplexity ? parseInt(perplexity, 10) : null,
    theta: theta ? parseInt(theta, 10) : null,
    outputFormat,
  }));
}

function processRtabFile(req) {
  const { rtabMatrix, perplexity, theta } = req;
  console.log('processRtabFile', rtabMatrix.length, perplexity, theta);
  return bhtsne(rtabMatrix, { perplexity, theta })
    .then(
      results => ({ results, req })
    );
}

function processDistFile(req) {
  const { rtabMatrix, perplexity, theta } = req;
  return bhtsne(rtabMatrix, { perplexity, theta })
    .then(
      results => ({ results, req })
    );
}

function formatResponse({ results, req }) {
  if (results) {
    const { outputFormat = 'json' } = req;
    if (outputFormat === 'dot') {
      return createGraph(results.ids, results.rows);
    }
    return results.rows.map(
      (row, index) => ({
        id: results.ids[index],
        x: row[0],
        y: row[1],
      })
    );
  }
  return { error: 'No results' };
}

const apiRouter = express.Router();

apiRouter
  .options('/', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'POST');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.status(200);
    next();
  })
  .post('/rtab', (req, res, next) => {
    console.log('Got request for processing rtab file');
    Promise.resolve(req)
      .then(getRequestParams)
      .then(processRtabFile)
      .then(formatResponse)
      .then(results => res.status(200).send(results))
      .catch(err => next(err));
  })
  .post('/dist', (req, res, next) => {
    console.log('Got request for processing distance matrix file');
    Promise.resolve(req)
      .then(getRequestParams)
      .then(processDistFile)
      .then(formatResponse)
      .then(results => res.status(200).send(results))
      .catch(err => next(err));
  })
  .use((req, res) => {
    res.sendStatus(404);
  });

module.exports = apiRouter;
