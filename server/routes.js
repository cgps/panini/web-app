/* eslint no-console: ["error", { allow: ["warn", "error"] }] */
/* eslint no-unused-vars: ["error", { "args": "none" }] */

const path = require('path');
const express = require('express');

const apiRouter = require('./controllers/api');

const publicPath = path.join(__dirname, '..', 'public');

module.exports = function (app) {
  app.use('/api/1.0', apiRouter);

  app.use((error, req, res, next) => {
    console.error('[express-error-handler]', error);
    if (error instanceof Error) {
      res.status(400).send({ error: error.message });
    } else {
      res.status(400).send({ error });
    }
  });

  app.use('/', (req, res, next) => {
    // crude file matching
    if (req.path.match(/\.[a-z]{1,4}$/) || req.xhr) {
      return next();
    }

    const config = app.config || {};

    const indexTemplateData = {
      webpack_hash: config.webpack_hash || '',
      googleAnalyticsAccount: config.googleAnalyticsAccount,
    };
    return res.render('pages/index', indexTemplateData);
  });

  app.use(express.static(publicPath));

  app.use((req, res) =>
   res.status(404).sendFile(path.join(publicPath, '404.html'))
  );
};
