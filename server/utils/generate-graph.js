const SCALE = 32;

function createGraph(ids, rows) {
  const lines = ids.map((id, i) => `"${id}"[x=${parseFloat(rows[i][0]) * SCALE},y=${parseFloat(rows[i][1]) * SCALE}]`);
  return `graph G { ${lines.join('; ')} }`;
}

module.exports = createGraph;
