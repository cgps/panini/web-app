/* eslint no-param-reassign: 0 */

const { reduceRtabDataAsync } = require('panini-node-native');

const defaults = {
  perplexity: 50,
  dimensions: 2,
  theta: 0.5,
  whitenD: 1000,
  whitenU: 100,
};

function parseRtabText(rtab, delimiter = '\t', newline = '\n') {
  const lines = (
    rtab
      .replace(/\r\n/g, newline)
      .replace(/\r/g, newline)
      .split(newline)
  );

  const ids = lines.shift().split(delimiter).map(x => x.trim());
  ids.shift();
  if (lines[lines.length - 1] === '') {
    lines.pop();
  }
  for (let i = 0; i < lines.length; i++) {
    if (lines[i] === '') {
      throw new Error(`Empty line at ${i}`);
    } else {
      lines[i] = lines[i].split(delimiter);
      lines[i].shift();
      let hasZeros = false;
      for (let j = 0; j < lines[i].length; j++) {
        lines[i][j] = lines[i][j].trim() === '0' ? 0 : 1;
        if (lines[i][j] === 0) {
          hasZeros = true;
        }
      }
      if (hasZeros === false) {
        lines.splice(i, 1);
        i--;
      }
    }
  }
  return { ids, matrix: lines };
}

function parseRtabMatrix(lines) {
  // first line contains
  const ids = lines.shift().map(id => id.trim());
  ids.shift();

  // remove last line if it is empty
  if (lines[lines.length - 1] === '') {
    lines.pop();
  }

  for (let i = 0; i < lines.length; i++) {
    if (lines[i] === '') {
      throw new Error(`Empty line at ${i}`);
    } else if (lines[i].length !== ids.length + 1) {
      throw new Error(`Wrong field count line at ${i} expected ${ids.length + 1} but found ${lines[i].length}`);
    } else {
      lines[i].shift();
      let hasZeros = false;
      for (let j = 0; j < lines[i].length; j++) {
        lines[i][j] = lines[i][j].trim() === '0' ? 0 : 1;
        if (lines[i][j] === 0) {
          hasZeros = true;
        }
      }
      if (hasZeros === false) {
        lines.splice(i, 1);
        i--;
      }
    }
  }
  return { ids, matrix: lines };
}

function parseRtabInput(matrix) {
  if (Array.isArray(matrix)) {
    return parseRtabMatrix(matrix);
  } else if (typeof matrix === 'string') {
    return parseRtabText(matrix);
  }
  throw new Error('Invalid input type');
}

function runRtab(rtabMatrix, { perplexity, dimensions, theta, whitenD, whitenU }) {
  console.log('Rtab request', { contents: rtabMatrix.length, perplexity, dimensions, theta, whitenD, whitenU });

  const { ids, matrix } = parseRtabInput(rtabMatrix);

  if (ids.length < 100) {
    throw new Error('The number of isolates should be greater than or equals 100.');
  }

  return new Promise((resolve, reject) => {
    const numberOfGenes = matrix.length;
    const numberOfSamples = matrix[0].length;
    const maxP = parseInt(numberOfSamples / 3, 10) - 1;

    reduceRtabDataAsync(matrix, numberOfGenes, numberOfSamples,
      whitenD || defaults.whitenD,
      whitenU || defaults.whitenU,
      dimensions || defaults.dimensions,
      perplexity || (defaults.perplexity > maxP ? maxP : defaults.perplexity),
      theta || defaults.theta,
      (error, rows) => {
        if (error) {
          reject(error);
        } else {
          resolve({ ids, rows });
        }
      }
    );
  });
}

module.exports = runRtab;
