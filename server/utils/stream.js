const StringDecoder = require('string_decoder').StringDecoder;

function parse(stream, delimiter = '\t', newline = '\n') {
  const lines = [];
  let buffer = '';
  return new Promise((resolve, reject) => {
    const decoder = new StringDecoder('utf8');

    stream.on('data', (chunk) => {
      buffer += (decoder.write(chunk) || '').replace(/\r\n/g, newline);

      if (buffer.indexOf(newline) > 0) {
        const parts = buffer.split(newline);

        for (let i = 0; i < parts.length - 1; i++) {
          lines.push(parts[i].split(delimiter));
        }

        buffer = parts[parts.length - 1];
      }
    });

    stream.on('error', (err) => reject(err));

    stream.on('end', () => {
      if (buffer.length) {
        const parts = buffer.split(newline);
        for (let i = 0; i < parts.length; i++) {
          lines.push(parts[i].split(delimiter));
        }
      }
      buffer = null;
      resolve(lines);
    });
  });
}

module.exports = parse;
