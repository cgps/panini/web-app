const fs = require('fs');
const path = require('path');

// set NODE_ENV to production
process.env.NODE_ENV = 'production';

require('./server')(function (error, app) {
  if (error) {
    console.log('Application not started:');
    throw error;
  }
  app.config.webpack_hash = fs.readFileSync(
    path.join(__dirname, 'webpack.hash'), 'utf8'
  );
  require('./start')(app);
});
