#!/usr/bin/env bash
set -e

name=`yaml get package.json name`
version=`yaml get package.json version`
image=registry.gitlab.com/cgps/$name:$version

docker build \
  -f ./docker/production/Dockerfile \
  -t $image \
  .
