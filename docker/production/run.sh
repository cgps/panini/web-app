#!/usr/bin/env bash
set -e

name=`yaml get package.json name`
version=`yaml get package.json version`
image=registry.gitlab.com/cgps/$name:$version

if [ "$PORT" = "" ]
then
   port=8080
else
   port=$PORT
fi

docker run \
  -d \
  -p $port:80 \
  $image
