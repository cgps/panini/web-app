#!/usr/bin/env bash
set -e

name=`yaml get package.json name`
tag=dev
image=registry.gitlab.com/cgps/$name:$tag

docker build \
  -f ./docker/dev/Dockerfile \
  -t $image \
  .
