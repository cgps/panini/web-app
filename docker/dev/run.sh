#!/usr/bin/env bash
set -e

name=`yaml get package.json name`
image=registry.gitlab.com/cgps/$name:dev

echo $image

if [ "$PORT" = "" ]
then
   port=8080
else
   port=$PORT
fi

docker run \
  -it \
  --rm \
  -v $(pwd):/panini \
  -v $HOME/.ssh:/root/.ssh \
  -p $port:8080 \
  -p 8888:8888 \
  -p 5858:5858 \
  -w /panini \
  $image \
  bash
